var app = angular.module("appcolaborate", ["ngRoute", "multipleSelect"]);
app.config(function($routeProvider) {
    $routeProvider
        .when("/", { templateUrl: "../views/login.html", controller: "ctrlLogin" })
        .when("/teams", { templateUrl: "../views/teams.html", controller: "ctrlTeams" })
        .when("/members", { templateUrl: "../views/members.html", controller: "ctrlMembers" })
        .when("/board", { templateUrl: "../views/board.html", controller: "ctrlBoard" })
        .when("/404", { templateUrl: "../views/404.html" }).otherwise({ redirectTo: "/404" });
});

app.factory("myServicio", function() {
    return { datos: {} };
});

app.controller("ctrlLogin", function($scope, $http, $location, myServicio) {
    $scope.pagina = "Login";
    $scope.usuario = [];
    $scope.seeLogin = true;

    $scope.showRegister = function() {
        $scope.usuario = [];
        if ($scope.seeLogin) {
            $scope.seeLogin = false;
            $scope.pagina = "Register";
        } else {
            $scope.seeLogin = true;
            $scope.pagina = "Login";
        }
    }

    $scope.iniciarSesion = function() {
        $http.post("/existUser", { correo: $scope.usuario.correo, contrasena: $scope.usuario.contrasena }).then(function(resulta) {
            if (JSON.stringify(resulta.data, null, 2).length == 2) {
                alert("Su correo o contrasena son errones porfavor verifiq de nuevo");
            } else {
                //		   		alert("Bienvenido "+resulta.data[0].nombre);
                myServicio.datos.usuario = resulta.data[0];
                $location.path("/teams");
            }
        });
    }
    $scope.registrarse = function() {
        //alert($scope.usuario.nombre);
        $http.post("/setUser", { nombre: $scope.usuario.nombre, correo: $scope.usuario.correo, imagen: $scope.usuario.imagen, contrasena: $scope.usuario.contrasena }).then(function() {
            alert("usuario registrado");
        });
        $scope.usuario = [];
    }

});


app.controller("ctrlTeams", function($scope, myServicio, $location, $http) {
    ///si no esta logueado redirecciona
    if (typeof(myServicio.datos.usuario) === "undefined") {
     $location.path("/");
	}
    $scope.usuario = myServicio.datos.usuario;// = { _id: "5a0ebc3b0d76b442bc77888c", nombre: "yara", correo: "yara@gmail.com", imagen: "https://bootdey.com/img/Content/use…", contrasena: "12345", __v: 0 };
    alert("Bienvenido "+$scope.usuario.nombre);
    $scope.pagina="teams";
    $scope.equipo=[];
    $scope.equipos=[];
    actualizarVista();

    $scope.setTeam=function(){
        setEquipo();
    }

    function setEquipo(){
        $http.post("/setTeam",{"idusuario":$scope.usuario._id,"nombre":$scope.equipo.nombre,"imagen":$scope.equipo.imagen}).then(function(res){
            //alert("equipo "+JSON.stringify(res.data,null,2));
            setMiembro(res.data._id);
        });
    }

    function setMiembro(idequipo){
        $http.post("/setMember",{"idusuario":$scope.usuario._id,"idequipo":idequipo}).then(function(resul){
            //alert("miembro "+JSON.stringify(resul.data,null,2));
            actualizarVista();
            alert("equipo nuevo creado");
        });
    }

    function actualizarVista(){
        $http.post("/getTeamsForMember",{"idusuario":$scope.usuario._id}).then(function(result){
            $scope.equipos=result.data;
        });
    }

    $scope.addMembersView=function(idteam){
        myServicio.datos.idequipo=idteam;
        $location.path("/members");
    }
    
    $scope.sendBoardView=function(team){
        myServicio.datos.equipo=team;
        myServicio.datos.usuario=$scope.usuario;
        $location.path("/board");
    }

});

app.controller("ctrlMembers", function($scope, $http, myServicio) {
    $scope.idteam = myServicio.datos.idequipo;
    alert($scope.idteam);
    $scope.selectedList2 = [];
    $scope.listaMiembros = [];
    getMiembros();
    $scope.apiUsers = "http://localhost:5000/getAllUsers";

    function getMiembros() {
        $http.post("/getAllMembersForTeam", { "idequipo": $scope.idteam }).then(function(result) {
            $scope.listaMiembros = result.data;
            console.log($scope.listaMiembros);
        });
    }

    $scope.setMembers= function() {
        if ($scope.selectedList2.length > 0) {
            for (var i = 0, length1 = $scope.selectedList2.length; i < length1; i++) {
                //console.log($scope.selectedList2[i]);
                $http.post("/setMember", { "idequipo": $scope.idteam, "idusuario": $scope.selectedList2[i]._id }).then(function(res) {
                    getMiembros();
                });
            };
            $scope.selectedList2 = [];
            alert("usuarios agregado");
        } else {
            alert("no se ha seleccionado ningun usuario");
        }

    }

    $scope.borrarMiembro=function(idmiembro){
        $http.post("/removeMember",{_id:idmiembro}).then(function(reul){
            //alert(JSON.parse(reul,null,2));
            getMiembros();
            alert("usuario eliminado");
        });
    }
});

app.controller("ctrlBoard",function($scope,myServicio){
    $scope.pagina="board bievenido";
    $scope.equipo=myServicio.datos.equipo;
    $scope.usuario=myServicio.datos.usuario;
    
    // var socket=io.connect("http://localhost:5000");
    var socket=io.connect();
    
/*    socket.on('news', function (data) {
        console.log(data); 
        alert(JSON.stringify(data,null,2));
    });*/

    // on connection to server, ask for user's name with an anonymous callback
    socket.on('connect', function(){
        // call the server-side function 'adduser' and send one parameter (value of prompt)
        //socket.emit('adduser', prompt("What's your name?"));
        socket.emit('adduser',$scope.usuario.nombre);
    });

    // listener, whenever the server emits 'updatechat', this updates the chat body
    socket.on('updatechat', function (username, data) {
        $('#conversation').append('<b>'+username + ':</b> ' + data + '<br>');
    });

    // listener, whenever the server emits 'updateusers', this updates the username list
    socket.on('updateusers', function(data) {
        $('#users').empty();
        $.each(data, function(key, value) {
            $('#users').append('<div>' + key + '</div>');
        });
    });

    // on load of page
    $(function(){
        // when the client clicks SEND
        $('#datasend').click( function() {
            var message = $('#data').val();
            $('#data').val('');
            // tell server to execute 'sendchat' and send along one parameter
            socket.emit('sendchat', message);
        });

        // when the client hits ENTER on their keyboard
        $('#data').keypress(function(e) {
            if(e.which == 13) {
                $(this).blur();
                $('#datasend').focus().click();
            }
        });
    });


});