var express=require("express");
var app=express();
var bodyparser=require("body-parser");
app.use(express.static(__dirname+"/public"));

app.set("port",(process.env.PORT||5000));
var mongoose=require("mongoose");
var Schema=mongoose.Schema;
mongoose.Promise=global.Promise;
//mongoose.connect('mongodb://yas:academia1A@ds155315.mlab.com:55315/prueba',{useMongoCLiente:true}).then(()=>console.log("connectio succegull to daatabase")).catch((err)=>console.console.error(erro));

mongoose.connect('mongodb://yas:academia1A@ds155315.mlab.com:55315/prueba',{ useMongoClient: true }).then(()=>
console.log("conection successful!!")).catch((err)=>console.error(err));
var server=app.listen(app.get("port"),function(){
	console.log("server corriendo en el puerto 5000");
});
var schema=mongoose.Schema;

var usuarioSchema=new schema({nombre:String,correo:String,contrasena:String,imagen:String});
var usuarioModel=mongoose.model('usuarios',usuarioSchema);

var equipoSchema=new schema({nombre:String,imagen:String,cantidad:Number,creacion:Date,idusuario:{type:Schema.ObjectId,ref:"usuarios"}});
var equipoModel=mongoose.model('equipos',equipoSchema);


var miembroSchema=new schema({idequipo:{type:Schema.ObjectId,ref:"equipos"},idusuario:{type:Schema.ObjectId,ref:"usuarios"},entrada:Date});
var miembroModel=mongoose.model('miembros',miembroSchema);

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true })); 


///***********//login//******************//
app.post("/setUser",function(req,res){
	var usuario=new usuarioModel(req.body);
	usuario.save(function(err){
		if (err) {
			console.log("dato erro");	
		}else{
			console.log("dato guardado");
		}
	});
	console.log(req.body);
	res.send("ok");

});
app.post("/existUser",function(req,res){
	usuarioModel.find(req.body,function(err,usearis){
		if (err) {
			console.log("a ocurrido un erro");
		}
		res.send(usearis);
	});
});



app.get("/getAllUsers",function(req,res){
	usuarioModel.find({},function(err,usearis){
		if (err) {
			console.log("a ocurrido un erro");
		}
		res.send(usearis);
	});
});

///**************//teams///*******************///
app.post("/setTeam",function(req,res){
	console.log(req.body);
	var equipo=new equipoModel({nombre:req.body.nombre,imagen:req.body.imagen,cantidad:1,creacion:new Date(),idusuario:req.body.idusuario});
	equipo.save(function(err,resul){
		if (err) {
			console.log("error al guardar equipo");
		}else{
			console.log(resul);
			res.send(resul);
		}
	});
	
});


app.post("/getTeamsForMember",function(req,res){
	miembroModel.find({idusuario:req.body.idusuario},function(err,resul){
		//if (err) {}
		//res.send(resul);
		equipoModel.populate(resul,{path:"idequipo"},function(err,equipo){
			res.status(200).send(equipo);
		});
	});

});

///*********///members///***********************///
app.post("/setMember",function(req,res){
	var member=new miembroModel({idequipo:req.body.idequipo,idusuario:req.body.idusuario,entrada:new Date()});
	member.save(function(err,resu){
		if (err) {
			console.log('erro guardar miembor');
		}else{
			console.log(resu);
		}
	});
	res.send("ok");
});

app.get("/getAllMembers",function(req,res){
	miembroModel.find(req.body,function(err,resul){
		if (err) {}
		res.send(resul);
	})

});
	
app.post("/removeMember",function(req,res){
	miembroModel.remove(req.body,function(err,resul){
		if (err) {}
		res.send(resul);
	});
});

app.post("/getAllMembersForTeam",function(req,res){
	miembroModel.find(req.body,function(err,resul){
		usuarioModel.populate(resul,{path:"idusuario"},function(err,result){
			res.status(200).send(result);
		});
	});
});

var usernames = {};
var io=require("socket.io")(server);
io.on("connection",function(socket){
	console.log("conexion socket");
	socket.emit('news', { hello: 'world' });

	// when the client emits 'sendchat', this listens and executes
	socket.on('sendchat', function (data) {
		// we tell the client to execute 'updatechat' with 2 parameters
		io.sockets.emit('updatechat', socket.username, data);
	});

	// when the client emits 'adduser', this listens and executes
	socket.on('adduser', function(username){
		// we store the username in the socket session for this client
		socket.username = username;
		// add the client's username to the global list
		usernames[username] = username;
		// echo to client they've connected
		socket.emit('updatechat', 'SERVER', 'you have connected');
		// echo globally (all clients) that a person has connected
		socket.broadcast.emit('updatechat', 'SERVER', username + ' has connected');
		// update the list of users in chat, client-side
		io.sockets.emit('updateusers', usernames);
	});

	// when the user disconnects.. perform this
	socket.on('disconnect', function(){
		// remove the username from global usernames list
		delete usernames[socket.username];
		// update list of users in chat, client-side
		io.sockets.emit('updateusers', usernames);
		// echo globally that this client has left
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
	});

});

